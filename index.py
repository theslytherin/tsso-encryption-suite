from bottle import *
import os
import anas, affine, autokey, morse, pickle
path = os.path.join(os.getcwd(), 'index.html')
print(path)
@error(404)
def errkor(error):
    path = os.path.join(os.getcwd(), 'templates')
    path  = os.path.join(path, '404.tpl')
    print(path)
    return template(path)

@route('/index.html')
@route('/index')
def index(file = 'index.tpl'):
    path = os.path.join(os.getcwd(), 'templates')
    path  = os.path.join(path, 'index.tpl')
    print(path)
    return template(path)

@route('/')
@get('/affine.html')
def login_form(key1 = None, key2 = None, text = None, output = None):
    path = os.path.join(os.getcwd(), 'templates')
    path  = os.path.join(path, 'affineget.tpl')
    print(path)
    dic ={
        'key1' : key1,
        'key2' : key2,
        'text' : text,
        'output' : output
    }
    return template(path, **dic)


@post('/affine.html')
def login_submit():
    name = request.forms.get('text')
    a = request.forms.get('key1')
    b = request.forms.get('key2')
    enc = request.forms.get('Encrypt')
    dec = request.forms.get('Decrypt')
    print(a, b, enc, dec)
    dic ={
        'key1' : a,
        'key2' : b,
        'text' : name,
        'output' : ' '
    }
    if enc == 'on':
        output = affine.encrypt(name, [int(a), int(b)])
        dic['output'] = output
        path = os.path.join(os.getcwd(), 'templates')
        path  = os.path.join(path, 'affineget.tpl')
        return template(path, **dic)
    elif dec == 'on':
        output = affine.decrypt(name, [int(a), int(b)])
        dic['output'] = output
        path = os.path.join(os.getcwd(), 'templates')
        path  = os.path.join(path, 'affineget.tpl')
        return template(path,**dic)
    else:
        dic['output'] = 'Select one of the options, CYKA!!!'
        path = os.path.join(os.getcwd(), 'templates')
        path  = os.path.join(path, 'affineget.tpl')
        return template(path,**dic)

@get('/autokey.html')
def login_form():
    path = os.path.join(os.getcwd(), 'templates')
    path  = os.path.join(path, 'autokeyget.tpl')
    print(path)
    dic ={
        'key1' : '',
        'text' : '',
        'output' : ' '
    }
    return template(path, **dic)

@post('/autokey.html')
def login_submit():
    name = request.forms.get('text')
    a = request.forms.get('key1')
    enc = request.forms.get('Encrypt')
    dec = request.forms.get('Decrypt')
    print(a, enc, dec)
    dic ={
        'key1' : a,
        'text' : name,
        'output' : ' '
    }
    if enc == 'on':
        output = autokey.encrypt(a, name)
        dic['output'] = output
        path = os.path.join(os.getcwd(), 'templates')
        path  = os.path.join(path, 'autokeyget.tpl')
        return template(path, **dic)
    elif dec == 'on':
        output = autokey.decrypt(a, name)
        dic['output'] = output
        path = os.path.join(os.getcwd(), 'templates')
        path  = os.path.join(path, 'autokeyget.tpl')
        return template(path,**dic)
    else:
        dic['output'] = 'Select one of the options, CYKA!!!'
        path = os.path.join(os.getcwd(), 'templates')
        path  = os.path.join(path, 'autokeyget.tpl')
        return template(path,**dic)

@get('/morse.html')
def login_form():
    path = os.path.join(os.getcwd(), 'templates')
    path  = os.path.join(path, 'morse.tpl')
    print(path)
    dic ={
        'text' : '',
        'output' : ' '
    }
    return template(path, **dic)

@post('/morse.html')
def login_submit():
    name = request.forms.get('text')
    enc = request.forms.get('Encrypt')
    dec = request.forms.get('Decrypt')
    print(enc, dec)
    dic ={
        'text' : name,
        'output' : ' '
    }
    if enc == 'on':
        output = morse.encrypt(name)
        dic['output'] = output
        path = os.path.join(os.getcwd(), 'templates')
        path  = os.path.join(path, 'morse.tpl')
        return template(path, **dic)
    elif dec == 'on':
        output = morse.decrypt(name)
        dic['output'] = output
        path = os.path.join(os.getcwd(), 'templates')
        path  = os.path.join(path, 'morse.tpl')
        return template(path,**dic)
    else:
        dic['output'] = 'Select one of the options, CYKA!!!'
        path = os.path.join(os.getcwd(), 'templates')
        path  = os.path.join(path, 'morse.tpl')
        return template(path,**dic)

if os.environ.get('APP_LOCATION') == 'heroku':
    run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
else:
    run(host='localhost', port=8080, debug = True)