A very basic encryption suite. Ideal for encrypting love letters, but only if your friends aren't savvy. Do not use this to encrypt things that pertain to your finances. 

See the app in action at https://tsso.herokuapp.com
