# Dictionary to lookup the index of alphabets
import spacer
dict1 = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4,
         'F': 5, 'G': 6, 'H': 7, 'I': 8, 'J': 9,
         'K': 10, 'L': 11, 'M': 12, 'N': 13, 'O': 14,
         'P': 15, 'Q': 16, 'R': 17, 'S': 18, 'T': 19,
         'U': 20, 'V': 21, 'W': 22, 'X': 23, 'Y': 24, 'Z': 25}

dict2 = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E',
         5: 'F', 6: 'G', 7: 'H', 8: 'I', 9: 'J',
         10: 'K', 11: 'L', 12: 'M', 13: 'N', 14: 'O',
         15: 'P', 16: 'Q', 17: 'R', 18: 'S', 19: 'T',
         20: 'U', 21: 'V', 22: 'W', 23: 'X', 24: 'Y', 25: 'Z'}

def encrypt(key, message):
    message = message.upper()
    message, loc = spacer.spacer(message)
    key = key.upper()
    keydash = key + message
    keystream = keydash[:len(message)]
    out = ""
    for i in zip(keystream,message):
        if i[1] == ' ':
            print('spaces not allowed')
        else:
            sum = dict1[i[1]] + dict1[i[0]]
            temp = dict2[sum%26]
            out += temp
    return spacer.despacer(out, loc)

def decrypt(key, message):
    message = message.upper()
    message, loc = spacer.spacer(message)
    key = key.upper()
    keystream = key
    out = ""
    for i in range(len(message)):
        diff = dict1[message[i]] - dict1[keystream[i]]
        if diff < 0:
            diff = diff + 26
        temp = dict2[diff%26]
        keystream += temp
        out += temp
    return spacer.despacer(out, loc)

def encrypt_file(file_in, file_out, key):
	f = open(file_out, 'a')
	with open(file_in, 'r') as filetoberead:
		j = filetoberead.readlines()
		for i in j:
			t = encrypt(key, i.rstrip())
			f.write(t)
			f.write('\n')
	f.close()

def decrypt_file(file_in, file_out, key):
	f = open(file_out, 'a')
	with open(file_in, 'r') as filetoberead:
		j = filetoberead.readlines()
		for i in j:
			t = decrypt(key, i.rstrip())
			f.write(t)
			f.write('\n')
	f.close()
