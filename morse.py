# Python program to implement Morse Code Translator 

''' 
VARIABLE KEY 
'cipher' -> 'stores the morse translated form of the english string' 
'decipher' -> 'stores the english translated form of the morse string' 
'citext' -> 'stores morse code of a single character' 
'i' -> 'keeps count of the spaces between morse characters' 
'message' -> 'stores the string to be encoded or decoded' 
'''
def spacer(text):
    d = []
    txt = list(text)
    for i in range(len(text)):
        if not((text[i].isalpha()) or text[i] == ' ') :
            d.append([i, text[i]])
            txt.remove(text[i])
    st = ""
    for i in txt:
        st += i
    return st, d 
    

def despacer(text, loc):
    t = list(text)
    for i in loc:
        t.insert(i[0],i[1])
    s = ''
    for i in t:
        s += i
    return s
# Dictionary representing the morse code chart 
MORSE_CODE_DICT = { 'A':'.-', 'B':'-...', 
					'C':'-.-.', 'D':'-..', 'E':'.', 
					'F':'..-.', 'G':'--.', 'H':'....', 
					'I':'..', 'J':'.---', 'K':'-.-', 
					'L':'.-..', 'M':'--', 'N':'-.', 
					'O':'---', 'P':'.--.', 'Q':'--.-', 
					'R':'.-.', 'S':'...', 'T':'-', 
					'U':'..-', 'V':'...-', 'W':'.--', 
					'X':'-..-', 'Y':'-.--', 'Z':'--..', 
					'1':'.----', '2':'..---', '3':'...--', 
					'4':'....-', '5':'.....', '6':'-....', 
					'7':'--...', '8':'---..', '9':'----.', 
					'0':'-----', ', ':'--..--', '.':'.-.-.-', 
					'?':'..--..', '/':'-..-.', '-':'-....-', 
					'(':'-.--.', ')':'-.--.-', ' ':' '} 
MORSE_TO_ENG = {}
for i in MORSE_CODE_DICT:
	MORSE_TO_ENG[MORSE_CODE_DICT[i]] = i


def encrypt(msg):
	m = msg.upper()
	res = ''
	for i in m:
		if i.isalpha() or i.isnumeric():
			res += MORSE_CODE_DICT[i] + ';'
		elif i == ' ':
			res += ' ;'
	return res


def decrypt(msg):
	msk = msg.split(";")
	res = ""
	for i in msk:
		if i == '':
			continue
		res += MORSE_TO_ENG[i]
	return res

def encrypt_file(file_in, file_out):
	f = open(file_out, 'a')
	with open(file_in, 'r') as filetoberead:
		j = filetoberead.readlines()
		for i in j:
			t = encrypt(i.rstrip())
			f.write(t)
			f.write('\n')
	f.close()

def decrypt_file(file_in, file_out):
	f = open(file_out, 'a')
	with open(file_in, 'r') as filetoberead:
		j = filetoberead.readlines()
		for i in j:
			t = decrypt(i.rstrip())
			f.write(t)
			f.write('\n')
	f.close()
