<!DOCTYPE html>
<html>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="theslytherin.gitlab.io/css/site.css">
    <title>TSSO Encryption Utilty &middot; Affine</title>
    <link rel='favicon' href = "favicon.ico">
</html>
<body>
    <div id="headerbar">
        <a id="kerb" href="index.html">TSSO Encryption Utilty</a>
        <div id="navbar">
            <a href="affine.html" class="navbutton current" id="aboutButton">affine </a> &nbsp;
            <a href="autokey.html" class="navbutton" id="blogButton">autokey </a> &nbsp;
            <a href="morse.html" class="navbutton" id="projectsButton">morse </a> &nbsp;
            <a href="theslytherin.gitlab.io" class="navbutton" id="projectsButton">website</a>
        </div>
    </div>
    <hr>

    <br>
    
    <div style="align-content: center ;font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;" class = "content">
        <form method="POST" action="/affine.html">
            <div>
                Encryption Key (a) :: <input name="key1" type="text" value = {{key1 or ''}} />
                <small>Must be coprime with 26</small>
            </div>
            <br>
            <div>
                Encryption Key (b) :: <input name="key2" type="text" value = {{key2 or ''}}/> <input type="checkbox"  name = "Encrypt" label = "Encrypt"/> Encrypt <input type = "checkbox" name = "Decrypt"/> Decrypt
                <br>
            </div>
            <br>
            <div>
                <textarea name = 'text' rows = '10' cols = '45'>{{text or ''}}</textarea>
                <textarea name = 'out' rows = '10' cols = '45'>{{output or ''}}</textarea>
                <br>
            </div>
            
            <input style="align-content: center;" type="submit" text="Encrypt"/>
          </form>
    </div>
    
    <div id="footer">
        <a href="gitlab.com/theslytherin" target="_blank">
          <img class="symbol" src="theslytherin.gitlab.io/assets/github.png" alt="theslytherin on gitlab">
        </a>
    
        <a href="http://www.keybase.io/theslytherin" target="_blank">
          <img class="symbol" src="theslytherin.gitlab.io/assets/keyb.png" alt="(C) keybase">
        </a>

        <a href="mailto:rahuljha.newdelhi@gmail.com" target="_blank">
            <img class="symbol" src="theslytherin.gitlab.io/assets/gmail.svg" alt="(C) keybase">
          </a>

          
    </div>