<!DOCTYPE html>
<html>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="theslytherin.gitlab.io/css/site.css">
    <title>TSSO Encryption Utilty &middot; Morse</title>
    <link rel='favicon' href = "favicon.ico">
</html>
<body>
    <div id="headerbar">
        <a id="kerb" href="index.html">TSSO Encryption Utilty </a>
        <div id="navbar">
            <a href="affine.html" class="navbutton" id="aboutButton">affine </a> &nbsp;
            <a href="autokey.html" class="navbutton" id="blogButton">autokey </a> &nbsp;
            <a href="morse.html" class="navbutton current" id="projectsButton">morse </a> &nbsp;
            <a href="theslytherin.gitlab.io" class="navbutton" id="projectsButton">website</a>
        </div>
    </div>
    <hr>

    <br>
    
    <div style="align-content: center ;font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;" class = "content">
        <form method="POST" action="/morse.html">
            <div>
                <input type="checkbox"  name = "Encrypt" label = "Encrypt"/> Encrypt <input type = "checkbox" name = "Decrypt"/> Decrypt
            </div>
            <br>
    <br>
            <div>
                <textarea name = 'text' rows = '10' cols = '45'>{{text or ''}}</textarea>
                <textarea name = 'out' rows = '10' cols = '45'>{{output or ''}}</textarea>
                <br>
            </div>
            
            <input style="align-content: center;" type="submit" text="Encrypt"/>
          </form>
    </div>
    <div id="footer">
        <a href="gitlab.com/theslytherin" target="_blank">
          <img class="symbol" src="theslytherin.gitlab.io/assets/github.png" alt="theslytherin on gitlab">
        </a>
    
        <a href="http://www.keybase.io/theslytherin" target="_blank">
          <img class="symbol" src="theslytherin.gitlab.io/assets/keyb.png" alt="(C) keybase">
        </a>

        <a href="mailto:rahuljha.newdelhi@gmail.com" target="_blank">
            <img class="symbol" src="theslytherin.gitlab.io/assets/gmail.svg" alt="(C) keybase">
          </a>

          
    </div>
