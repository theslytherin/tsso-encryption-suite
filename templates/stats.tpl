<!DOCTYPE html>
<html>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="theslytherin.gitlab.io/css/site.css">
    <title>TSSO Encryption Utilty</title>
    <link rel='favicon' href = "favicon.ico">
</html>

<body>
    <div id="headerbar">
        <a id="kerb" href="index.html">TSSO Encryption Utilty</a>
        <div id="navbar">
            <a href="affine.html" class="navbutton" id="aboutButton">affine </a> &nbsp;
            <a href="autokey.html" class="navbutton" id="blogButton">autokey </a> &nbsp;
            <a href="morse.html" class="navbutton" id="projectsButton">morse </a> &nbsp;
            <a href="theslytherin.gitlab.io" class="navbutton" id="projectsButton">website</a>
        </div>
    </div>
    <hr>
    <div class="content" id="about">
        <ul>
            <li>Total page visits : {{total}}</li>
            <li>Affine : {{affine}}</li>
            <li>Autokey : {{autokey}}</li>
            <li>Morse : {{morse}}</li>
        </ul>
    </div>
        <div id="footer">
            <a href="gitlab.com/theslytherin" target="_blank">
              <img class="symbol" src="theslytherin.gitlab.io/assets/github.png" alt="theslytherin on gitlab">
            </a>
            
            <a href="http://www.keybase.io/theslytherin" target="_blank">
              <img class="symbol" src="theslytherin.gitlab.io/assets/keyb.png" alt="(C) keybase">
            </a>

            <a href="mailto:rahuljha.newdelhi@gmail.com" target="_blank">
                <img class="symbol" src="theslytherin.gitlab.io/assets/gmail.svg" alt="(C) keybase">
              </a>

              <a href="/stats" class = "blogButton"> Stats </a>
        
    </div>
        
</body>   
        
