<!--Blatantly copied from shardulc-->
<!DOCTYPE html>
<html>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="theslytherin.gitlab.io/css/site.css">
    <title>TSSO Encryption Utilty</title>
    <link rel='favicon' href = "favicon.ico">
</html>

<body>
    <div id="headerbar">
        <a id="kerb" href="index.html">TSSO Encryption Utilty</a>
        <div id="navbar">
            <a href="affine.html" class="navbutton" id="aboutButton">affine </a> &nbsp;
            <a href="autokey.html" class="navbutton" id="blogButton">autokey </a> &nbsp;
            <a href="morse.html" class="navbutton" id="projectsButton">morse </a> &nbsp;
            <a href="theslytherin.gitlab.io" class="navbutton" id="projectsButton">website</a>
        </div>
    </div>
    <hr>
    <div class="content" id="about">
        
    <div class="content" id="about">
        This is a (rather crappy, I must admit) web app implementation of my CS Project.
        <br>
        <br>
        <strong><code> $ Backstory | Brief</code></strong>
        <br>
        <br>
        When I was in 11th grade, I wrote an Encryption algorithm called ANAS in order to chat with my friends. Those were the good old days, when mobile
        phones were contraband in school, so this algorithm could keep our top secret messages safe. The only problem was that no one in my class (including me) was smart
        enough to do the calculations by hand. I overcame the problem by implementing the algorithm in every computer of our computer lab (dumb, ik).
        Then in 12th grade, when I had to pick a project, I chose this utility and wrote a buggy GUI. And today, 
        I am making the project accessible to everyone by making a web app!



        <div class = "content">
            Even though the web app is marvellous in many ways, it does not support file encryption (because I do not want to <b>pay</b> for it, you are welcome to buy me a subscription).
            If you have got files to encrypt, download the implementation from 
            <a href="https://mega.nz/folder/cAQlxC4Y#DPnUoFfSeBWCF58HZZI-PQ"> here.</a>
        </div>
        <br>
        <br>

        <div id="propicDiv" style="align-self: flex-end;">
            <a href="theslytherin.gitlab.io/assets/profile.jpg">
                <img src="theslytherin.gitlab.io/assets/profile.jpg" alt="profile picture" caption= "Fairly accurate pictorial representation of Rahul Jha" id="propic">
            </a>
            
        </div>
        <strong>
           Finally, I know I did not need to put my picture on this page, but I did that just to torture you.
        </strong>
    </div>
    <div id="footer">
            <a href="gitlab.com/theslytherin" target="_blank">
              <img class="symbol" src="theslytherin.gitlab.io/assets/github.png" alt="theslytherin on gitlab">
            </a>
        
            <a href="http://www.keybase.io/theslytherin" target="_blank">
              <img class="symbol" src="theslytherin.gitlab.io/assets/keyb.png" alt="(C) keybase">
            </a>

            <a href="mailto:rahuljha.newdelhi@gmail.com" target="_blank">
                <img class="symbol" src="theslytherin.gitlab.io/assets/gmail.svg" alt="(C) keybase">
              </a>

            
            
        
    </div>
        
</body>