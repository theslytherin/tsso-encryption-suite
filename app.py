from tkinter import *
from tkinter import ttk 
import pyperclip

def morse(window, text, m):
    import morse
    if m == 'e':
        res = morse.encrypt(text.rstrip())
    elif m == 'd':
        res = morse.decrypt(text.rstrip())
    t3 = Text(window, width = 15, height = 2, wrap = "none")
    try:
        t3.delete(0)
    except:
        pass
    t3.insert(INSERT, res)
    pyperclip.copy(res)
    t3.grid(row = 4, column = 3)

def anas(window, key, text, m):
    import anas
    if m == 'e':
        res = anas.encrypt(text, key)
    elif m == 'd':
        res = anas.decrypt(text, key)
    t3 = Text(window, width = 15, height = 2, wrap = "none")
    try:
        t3.delete(0)
    except:
        pass
    t3.insert(INSERT, res)
    pyperclip.copy(res)
    t3.grid(row = 4, column = 3)

def autokey(window, key, text, m):
    import autokey
    if m == 'e':
        res = autokey.encrypt(key.rstrip(), text.lstrip())
    elif m == 'd':
        res = autokey.decrypt(key.rstrip(), text.lstrip())
    t3 = Text(window, width = 15, height = 2, wrap = "none")
    try:
        t3.delete(0)
    except:
        pass
    t3.insert(INSERT, res)
    pyperclip.copy(res)
    t3.grid(row = 4, column = 3)

def affine(window, t1, t2, m):
    import affine
    if m == 'e':
        res = affine.encrypt(t2, t1)
    elif m == 'd':
        res = affine.decrypt(t2, t1)
    t3 = Text(window, width = 15, height = 2, wrap = "none")
    try:
        t3.delete(0)
    except:
        pass
    t3.insert(INSERT, res)
    pyperclip.copy(res)
    t3.grid(row = 4, column = 3)

def anas_file(window, file_in, file_out, key, m):
    import anas
    if m == 'e':
        anas.encrypt_file(file_in, file_out, key)
    elif m == 'd':
        anas.decrypt_file(file_in, file_out, key)
    t3 = Label(window, text = "File operation completed!!")
    t3.grid(row = 5, column = 3)

def autokey_file(window, file_in, file_out, key, m):
    import autokey
    if m == 'e':
        autokey.encrypt_file(file_in, file_out, key)
    elif m == 'd':
        autokey.decrypt_file(file_in, file_out, key)
    t3 = Label(window, text = "File operation completed!!")
    t3.grid(row = 5, column = 3)

def affine_file(window, file_in, file_out, key, m):
    import affine
    if m == 'e':
        affine.encrypt_file(file_in, file_out, key)
    elif m == 'd':
        affine.decrypt_file(file_in, file_out, key)
    t3 = Label(window, text = "File operation completed!!")
    t3.grid(row = 5, column = 3)

def morse_file(window, file_in, file_out, m):
    import morse
    if m == 'e':
        morse.encrypt_file(file_in, file_out)
    elif m == 'd':
        morse.decrypt_file(file_in, file_out)
    t3 = Label(window, text = "File operation completed!!")
    t3.grid(row = 5, column = 3)
    

def update(window, t1, t2):
    p3 = Label(window, pady = 0.01)
    p3.grid(row=2)
    if (t1 == "Encrypt"):
        if (t2 == "Autokey"):
            l1 = Label(window, text = "Key")
            l1.grid(row = 3, column = 0)
            t = Text(window, width = 10, height = 0.4, wrap = "none")
            t.grid(row = 3, column = 1)
            t2 = Text(window, width = 15, height = 20, wrap = "none")
            t2.grid(row = 4, column = 1)
            btn2 = Button(window, text="Encrypt", command = lambda : autokey(window, t.get('1.0', 'end'), t2.get('1.0', 'end'), 'e'))
            btn2.grid(row = 3, column = 3)
        
        elif (t2 == "Affine"):
            l1 = Label(window, text = "Key (a)")
            l1.grid(row = 3, column = 0)
            l2 = Label(window, text = "Key (b)")
            l2.grid(row = 3, column = 2)
            ta = Text(window, width = 10, height = 0.4, wrap = "none")
            ta.grid(row = 3, column = 1)
            tb = Text(window, width = 10, height = 0.4, wrap = "none")
            tb.grid(row = 3, column = 3)
            t2 = Text(window, width = 15, height = 20, wrap = "none")
            t2.grid(row = 4, column = 1)
            btn2 = Button(window, text="Encrypt", command = lambda : affine(window, [int(ta.get('1.0', 'end').rstrip()), int(tb.get('1.0', 'end').rstrip())], t2.get('1.0', 'end'),'e'))
            btn2.grid(row = 3, column = 4)

        elif (t2 == "3661"):
            l1 = Label(window, text = "Key")
            l1.grid(row = 3, column = 0)
            t = Text(window, width = 10, height = 0.4, wrap = "none")
            t.grid(row = 3, column = 1)
            t2 = Text(window, width = 15, height = 20, wrap = "none")
            t2.grid(row = 4, column = 1)
            btn2 = Button(window, text="Encrypt", command = lambda : anas(window, int(t.get('1.0', 'end').rstrip()), t2.get('1.0', 'end'), 'e'))
            btn2.grid(row = 3, column = 3)
        
        elif (t2 == "Morse"):
            l1 = Label(window, text = "Warning : Doesn't work with special characters!")
            l1.grid(row = 3, column = 0)
            t2 = Text(window, width = 15, height = 20, wrap = "none")
            t2.grid(row = 4, column = 1)
            btn2 = Button(window, text="Encrypt", command = lambda : morse(window,t2.get('1.0', 'end'), 'e'))
            btn2.grid(row = 3, column = 3)
    
    elif (t1 == "Decrypt"):
        if (t2 == "Autokey"):
            l1 = Label(window, text = "Key")
            l1.grid(row = 3, column = 0)
            t = Text(window, width = 10, height = 0.4, wrap = "none")
            t.grid(row = 3, column = 1)
            t2 = Text(window, width = 15, height = 20, wrap = "none")
            t2.grid(row = 4, column = 1)
            btn2 = Button(window, text="Decrypt", command = lambda : autokey(window, t.get('1.0', 'end'), t2.get('1.0', 'end'), 'd'))
            btn2.grid(row = 3, column = 3)

        elif (t2 == "Affine"):
            l1 = Label(window, text = "Key (a)")
            l1.grid(row = 3, column = 0)
            l2 = Label(window, text = "Key (b)")
            l2.grid(row = 3, column = 2)
            ta = Text(window, width = 10, height = 0.4, wrap = "none")
            ta.grid(row = 3, column = 1)
            tb = Text(window, width = 10, height = 0.4, wrap = "none")
            tb.grid(row = 3, column = 3)
            t2 = Text(window, width = 15, height = 20, wrap = "none")
            t2.grid(row = 4, column = 1)
            btn2 = Button(window, text="Decrypt", command = lambda : affine(window, [int(ta.get('1.0', 'end').rstrip()), int(tb.get('1.0', 'end').rstrip())], t2.get('1.0', 'end'),'d'))
            btn2.grid(row = 3, column = 4)

        elif (t2 == "3661"):
            l1 = Label(window, text = "Key")
            l1.grid(row = 3, column = 0)
            t = Text(window, width = 10, height = 0.4, wrap = "none")
            t.grid(row = 3, column = 1)
            t2 = Text(window, width = 15, height = 20, wrap = "none")
            t2.grid(row = 4, column = 1)
            btn2 = Button(window, text="Decrypt", command = lambda : anas(window, int(t.get('1.0', 'end').rstrip()), t2.get('1.0', 'end'), 'd'))
            btn2.grid(row = 3, column = 3)
        
        elif (t2 == "Morse"):
            l1 = Label(window, text = "Warning : Doesn't work with special chars!")
            l1.grid(row = 3, column = 0)
            t2 = Text(window, width = 15, height = 20, wrap = "none")
            t2.grid(row = 4, column = 1)
            btn2 = Button(window, text="Decrypt", command = lambda : morse(window,t2.get('1.0', 'end'), 'd'))
            btn2.grid(row = 3, column = 3)

    elif (t1 == 'Encrypt File'):
        if t2 == 'Autokey':
            l1 = Label(window, text = "Key")
            l1.grid(row = 3, column = 0)
            t1 = Text(window, width = 10, height = 0.4, wrap = "none")
            t1.grid(row = 3, column = 1)
            l2 = Label(window, text = 'Source')
            l2.grid(row = 4, column=0)
            t2 = Text(window, width = 10, height = 0.4, wrap = "none")
            t2.grid(row = 4, column = 1)
            l3 = Label(window, text = 'Destination')
            l3.grid(row = 4, column=2)
            t3 = Text(window, width = 10, height = 0.4, wrap = "none")
            t3.grid(row = 4, column = 3)
            btn2 = Button(window, text="Encrypt", command = lambda : autokey_file(window,t2.get('1.0', 'end').rstrip(), t3.get('1.0', 'end').rstrip(), t1.get('1.0' , 'end').rstrip(),'e'))
            btn2.grid(row = 5, column = 1)
        
        elif t2 == '3661':
            l1 = Label(window, text = "Key")
            l1.grid(row = 3, column = 0)
            t1 = Text(window, width = 10, height = 0.4, wrap = "none")
            t1.grid(row = 3, column = 1)
            l2 = Label(window, text = 'Source')
            l2.grid(row = 4, column=0)
            t2 = Text(window, width = 10, height = 0.4, wrap = "none")
            t2.grid(row = 4, column = 1)
            l3 = Label(window, text = 'Destination')
            l3.grid(row = 4, column=2)
            t3 = Text(window, width = 10, height = 0.4, wrap = "none")
            t3.grid(row = 4, column = 3)
            btn2 = Button(window, text="Encrypt", command = lambda : anas_file(window,t2.get('1.0', 'end').rstrip(), t3.get('1.0', 'end').rstrip(), t1.get('1.0' , 'end').rstrip(),'e'))
            btn2.grid(row = 5, column = 1)

        elif t2 == 'Affine':
            l1 = Label(window, text = "Key (a)")
            l1.grid(row = 3, column = 0)
            t1 = Text(window, width = 10, height = 0.4, wrap = "none")
            t1.grid(row = 3, column = 1)
            l2 = Label(window, text = 'Key (b)')
            l2.grid(row = 3, column=2)
            t2 = Text(window, width = 10, height = 0.4, wrap = "none")
            t2.grid(row = 3, column = 3)
            l3 = Label(window, text = 'Source')
            l3.grid(row = 4, column=0)
            t3 = Text(window, width = 10, height = 0.4, wrap = "none")
            t3.grid(row = 4, column = 1)
            l4 = Label(window, text = 'Destination')
            l4.grid(row = 4, column=2)
            t4 = Text(window, width = 10, height = 0.4, wrap = "none")
            t4.grid(row = 4, column = 3)
            btn2 = Button(window, text="Encrypt", command = lambda : affine_file(window,t3.get('1.0', 'end').rstrip(), t4.get('1.0', 'end').rstrip(), [int(t1.get('1.0' , 'end').rstrip()),int(t2.get('1.0', 'end').rstrip())],'e'))
            btn2.grid(row = 5, column = 1)
        
        elif t2 == 'Morse':
            l2 = Label(window, text = 'Source')
            l2.grid(row = 4, column=0)
            t2 = Text(window, width = 10, height = 0.4, wrap = "none")
            t2.grid(row = 4, column = 1)
            l3 = Label(window, text = 'Destination')
            l3.grid(row = 4, column=2)
            t3 = Text(window, width = 10, height = 0.4, wrap = "none")
            t3.grid(row = 4, column = 3)
            btn2 = Button(window, text="Encrypt", command = lambda : morse_file(window,t2.get('1.0', 'end').rstrip(), t3.get('1.0', 'end').rstrip(),'e'))
            btn2.grid(row = 5, column = 1)
        
    elif (t1 == 'Decrypt File'):
        if t2 == 'Autokey':
            l1 = Label(window, text = "Key")
            l1.grid(row = 3, column = 0)
            t1 = Text(window, width = 10, height = 0.4, wrap = "none")
            t1.grid(row = 3, column = 1)
            l2 = Label(window, text = 'Source')
            l2.grid(row = 4, column=0)
            t2 = Text(window, width = 10, height = 0.4, wrap = "none")
            t2.grid(row = 4, column = 1)
            l3 = Label(window, text = 'Destination')
            l3.grid(row = 4, column=2)
            t3 = Text(window, width = 10, height = 0.4, wrap = "none")
            t3.grid(row = 4, column = 3)
            btn2 = Button(window, text="Decrypt", command = lambda : autokey_file(window,t2.get('1.0', 'end').rstrip(), t3.get('1.0', 'end').rstrip(), t1.get('1.0' , 'end').rstrip(),'d'))
            btn2.grid(row = 5, column = 1)
        
        elif t2 == '3661':
            l1 = Label(window, text = "Key")
            l1.grid(row = 3, column = 0)
            t1 = Text(window, width = 10, height = 0.4, wrap = "none")
            t1.grid(row = 3, column = 1)
            l2 = Label(window, text = 'Source')
            l2.grid(row = 4, column=0)
            t2 = Text(window, width = 10, height = 0.4, wrap = "none")
            t2.grid(row = 4, column = 1)
            l3 = Label(window, text = 'Destination')
            l3.grid(row = 4, column=2)
            t3 = Text(window, width = 10, height = 0.4, wrap = "none")
            t3.grid(row = 4, column = 3)
            btn2 = Button(window, text="Decrypt", command = lambda : anas_file(window,t2.get('1.0', 'end').rstrip(), t3.get('1.0', 'end').rstrip(), t1.get('1.0' , 'end').rstrip(),'d'))
            btn2.grid(row = 5, column = 1)

        elif t2 == 'Affine':
            l1 = Label(window, text = "Key (a)")
            l1.grid(row = 3, column = 0)
            t1 = Text(window, width = 10, height = 0.4, wrap = "none")
            t1.grid(row = 3, column = 1)
            l2 = Label(window, text = 'Key (b)')
            l2.grid(row = 3, column=2)
            t2 = Text(window, width = 10, height = 0.4, wrap = "none")
            t2.grid(row = 3, column = 3)
            l3 = Label(window, text = 'Source')
            l3.grid(row = 4, column=0)
            t3 = Text(window, width = 10, height = 0.4, wrap = "none")
            t3.grid(row = 4, column = 1)
            l4 = Label(window, text = 'Destination')
            l4.grid(row = 4, column=2)
            t4 = Text(window, width = 10, height = 0.4, wrap = "none")
            t4.grid(row = 4, column = 3)
            btn2 = Button(window, text="Decrypt", command = lambda : affine_file(window,t3.get('1.0', 'end').rstrip(), t4.get('1.0', 'end').rstrip(), [int(t1.get('1.0' , 'end').rstrip()),int(t2.get('1.0', 'end').rstrip())],'d'))
            btn2.grid(row = 5, column = 1)
        
        elif t2 == 'Morse':
            l2 = Label(window, text = 'Source')
            l2.grid(row = 4, column=0)
            t2 = Text(window, width = 10, height = 0.4, wrap = "none")
            t2.grid(row = 4, column = 1)
            l3 = Label(window, text = 'Destination')
            l3.grid(row = 4, column=2)
            t3 = Text(window, width = 10, height = 0.4, wrap = "none")
            t3.grid(row = 4, column = 3)
            btn2 = Button(window, text="Encrypt", command = lambda : morse_file(window,t2.get('1.0', 'end').rstrip(), t3.get('1.0', 'end').rstrip(),'d'))
            btn2.grid(row = 5, column = 1)


def main():
    window = Tk()
    window.title("Encryption Utility")
    window.geometry("410x500")
    #l1 = Label(window, text = "Encryption Tool", font= "Digiface", padx = 100)
    #l1.grid(row=0, column = 1)
    #l2.grid(row=1, column=0, columnspan = 3)
    opt = Label(window, text= "Configuration", font = "Bahnschrift 10", padx=10)
    opt.grid(row =1, column=0)
    n = StringVar() 
    option = ttk.Combobox(window, width = 15, textvariable = n) 
    # Adding combobox drop down list 
    option['values'] = ('Encrypt','Decrypt', 'Encrypt File', 'Decrypt File')
    option.grid(row = 1, column = 1)
    option.current(0) 
    m = StringVar()
    option2 = ttk.Combobox(window, width = 15, textvariable = m) 
    # Adding combobox drop down list 
    option2['values'] = ('Autokey','Affine','3661', 'Morse')
    pad1 = Label(window, padx=5)
    pad1.grid(row =1, column =2)
    option2.grid(row = 1, column = 3)
    option2.current(0) 
    pad2 = Label(window, padx=5)
    pad2.grid(row =1, column =4)
    btn1 = Button(window, text="OK", command = lambda : update(window, n.get(), m.get()))
    btn1.grid(row = 1, column = 5)
    pad3 = Label(window, padx=5)
    pad3.grid(row =1, column =6)
    print(n.get(), m.get())
    window.mainloop()
main()